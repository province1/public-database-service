const express = require('express');
const bodyParser = require("body-parser");
const basicAuth = require('express-basic-auth');
const { body, validationResult, check } = require('express-validator');

const {query, last_query,  sum_aggregate, aggregate } = require('./lib/mongodb.js');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(basicAuth({
    users: { 'admin': process.env.AUTH }
}));


const db_name = process.env.NODE_ENV;
const port = process.env.PORT;

const allowed_collections = ["equipment", "land_data", "bids", "materials"];


app.post('/query', body("collection").isIn(allowed_collections),async function (req, res) {
  
  const errors = validationResult(req);
  
  if (!errors.isEmpty()) {

      var error_log_validation = {"error log": "/query", timestamp: Date.now(), query: req.body.query_search, collection, sort: req.body.sort, error: "validation error", details: errors.errors[0].value.timestamp};
      console.log( error_log_validation );
      
      res.json( {error: true, error_msg: "validation error", details: errors.errors} );
    
  } else {

      var collection = req.body.collection;
      var query_search = req.body.query;
      var sort = req.body.sort;
      var result = await query(db_name, collection, query_search, sort);
      res.json(result)
  }
  
});

app.post('/current_state', body("collection").isIn(allowed_collections),async function (req, res) {
  
  const errors = validationResult(req);
  
  if (!errors.isEmpty()) {

      var error_log_validation = {"error log": "/current_state", timestamp: Date.now(), query: req.body.query_search, collection, sort: req.body.sort, error: "validation error", details: errors.errors[0].value.timestamp};
      console.log( error_log_validation );
      
      res.json( {error: true, error_msg: "validation error", details: errors.errors} );
    
  } else {

      var collection = req.body.collection;
      var query_search = req.body.query;
      var sort_params = {timestamp: -1};
      
      console.log(77777, db_name, collection, query_search, sort_params)
      
      var result = await last_query(db_name, collection, query_search, sort_params, 1);
      
      console.log(888, result)
      
      res.json(result)
  }
  
});

app.post('/current_state_many', body("collection").isIn(allowed_collections), async function (req, res) {
  
  
  const errors = validationResult(req);
  
  

  if (!errors.isEmpty()) {

    var error_log_validation = {"error log": "/current_state_many", timestamp: Date.now(), filter: req.body.filter, error: "validation error", details: errors.errors[0].value.timestamp};
    console.log( error_log_validation );
    
    res.json( {error: true, error_msg: "validation error", details: errors.errors} );
    
  } else {

    var collection = req.body.collection;
    var filter = req.body.filter;
    var grouper = req.body.grouper;
    
    var result = await aggregate(db_name, collection, filter, grouper);
    res.json(result)
  }
  
});



app.post('/sum_aggregate', body("collection").isIn(allowed_collections), async function (req, res) {
  
  
  const errors = validationResult(req);
  
  if (!errors.isEmpty()) {

    var error_log_validation = {"error log": "/sum_aggregate", timestamp: Date.now(), entry_doc: req.body.entry_doc, error: "validation error", details: errors.errors[0].value.timestamp};
    console.log( error_log_validation );
    
    res.json( {error: true, error_msg: "validation error", details: errors.errors} );
    
  } else {

    var collection = req.body.collection;
    var filter = req.body.filter;
    
    var result = await sum_aggregate(db_name, collection, filter);
    res.json(result)
  }
  
});


app.listen(port, () => {
  console.log(  `${process.env.NAME} listening at http://localhost:${port}`);
});
