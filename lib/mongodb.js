const { MongoClient, ServerApiVersion } = require('mongodb');


const uri = process.env.MDB_URL;
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });

async function last_query(db_name, collection_name, query, sort_params, limit){
    
    try {
        await client.connect();
        const collection = client.db(db_name).collection(collection_name);
        var result = await collection.find(query).sort(sort_params).limit(limit).toArray();
        return {error: false, result};
        
    } catch (e) {
        var error_log = {"error log": "/query", timestamp: Date.now(), query: JSON.stringify(query), collection: JSON.stringify(collection_name), sort_params: JSON.stringify(sort_params), error: e.errmsg};
        console.error(error_log);
        return {error: true, error_msg: e.errmsg}
    } finally {
        
        client.close();

    }
    
    
}

async function query(db_name, collection_name, query, sort_params){
    
    try {
        await client.connect();
        const collection = client.db(db_name).collection(collection_name);
        var result = await collection.find(query).sort(sort_params).toArray()
        return {error: false, result}
        
    } catch (e) {
        
        var error_log = {"error log": "/query", timestamp: Date.now(), query: JSON.stringify(query), collection: JSON.stringify(collection_name), sort_params: JSON.stringify(sort_params), error: e.errmsg};
        console.error(error_log);
        return {error: true, error_msg: e.errmsg}
        
    } finally {
        
        client.close();

    }
    
    
}

async function aggregate(db_name, collection_name, filter, group){
    
    // ToDo: Fix the result + error handling
    
    var result_clean = {}
    
    try {
        await client.connect();
        const collection = client.db(db_name).collection(collection_name);
        
         var pipeline = [
          
            { $match: filter},
            { $sort: {timestamp: -1} },
            { $group: { _id: "$"+group, 
                        latest: { $first: "$$ROOT"}} },
            ];
        
        var result = await collection.aggregate(pipeline).toArray();
        
        result_clean = result.map(x => {
            return x.latest
        })
        

        
    } catch (e) {
        var error_log = {error_msg: e, error_code: 0, error: true};
        console.log(error_log);
        result_clean = {error_log}
        
    } finally {
        

        client.close();
        return result_clean;
        
    }
    
    
}

async function sum_aggregate(db_name, collection_name, filter){
    
    try {
    
        await client.connect();
        const collection = client.db(db_name).collection(collection_name);
        
         var pipeline = [
              
                { $match: filter},
                { $group: { _id: "null", 
                            total: { $sum: "$amount"}} },
        ];
        
        var result = await collection.aggregate(pipeline).toArray();
        
        client.close()
        
        return {error: false, result: result[0]}
        
    } catch(e) {
        
        var error_log = {"error log": "sub_aggregate", timestamp: Date.now(), filter, error: e.errmsg};
        console.error(error_log);
        
        return {error: true}
        
    } finally{
        client.close()
    }
    
    
    
}



module.exports = {last_query, query, aggregate, sum_aggregate}

